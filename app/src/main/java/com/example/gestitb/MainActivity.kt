package com.example.gestitb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    lateinit var nameText: TextView
    lateinit var nameEditText: EditText
    lateinit var detailsText: TextView
    lateinit var moduleSpinner: Spinner
    lateinit var dateButton: Button
    lateinit var justifiedCheck: CheckBox


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}